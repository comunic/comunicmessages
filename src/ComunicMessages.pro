QT += widgets network

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

SOURCES += \
    main.cpp \
    helpers/accounthelper.cpp \
    widgets/loginwidget.cpp \
    utils/accountutils.cpp \
    data/accountloginrequest.cpp \
    data/apirequest.cpp \
    data/apirequestparameter.cpp \
    helpers/apihelper.cpp \
    utils/jsonutils.cpp \
    data/apirequestslist.cpp \
    helpers/configurationhelper.cpp \
    data/accountlogintokens.cpp \
    widgets/mainwindow.cpp \
    widgets/aboutthisappdialog.cpp \
    controllers/initcontroller.cpp \
    data/conversation.cpp \
    helpers/conversationslisthelper.cpp \
    widgets/conversationslistwidget.cpp \
    widgets/conversationitemwidget.cpp \
    data/user.cpp \
    helpers/usershelper.cpp \
    data/conversationslist.cpp \
    utils/uiutils.cpp \
    data/userslist.cpp \
    utils/timeutils.cpp \
    widgets/conversationwidget.cpp \
    data/newconversationmessage.cpp \
    helpers/conversationhelper.cpp \
    data/conversationmessage.cpp \
    widgets/conversationmessagewidget.cpp \
    data/conversationmessageslist.cpp \
    helpers/imageloadhelper.cpp \
    utils/filesutils.cpp \
    widgets/remoteimagemanager.cpp \
    widgets/clickablelabel.cpp \
    widgets/currentuserinformationwidget.cpp

HEADERS += \
    helpers/accounthelper.h \
    config.h \
    widgets/loginwidget.h \
    utils/accountutils.h \
    data/accountloginrequest.h \
    data/apirequest.h \
    data/apirequestparameter.h \
    helpers/apihelper.h \
    utils/jsonutils.h \
    data/apirequestslist.h \
    helpers/configurationhelper.h \
    data/accountlogintokens.h \
    widgets/mainwindow.h \
    widgets/aboutthisappdialog.h \
    controllers/initcontroller.h \
    data/conversation.h \
    helpers/conversationslisthelper.h \
    widgets/conversationslistwidget.h \
    widgets/conversationitemwidget.h \
    data/user.h \
    helpers/usershelper.h \
    data/conversationslist.h \
    utils/uiutils.h \
    data/userslist.h \
    utils/timeutils.h \
    widgets/conversationwidget.h \
    data/newconversationmessage.h \
    helpers/conversationhelper.h \
    data/conversationmessage.h \
    widgets/conversationmessagewidget.h \
    data/conversationmessageslist.h \
    helpers/imageloadhelper.h \
    utils/filesutils.h \
    data/qlabelholder.h \
    widgets/remoteimagemanager.h \
    widgets/clickablelabel.h \
    widgets/currentuserinformationwidget.h

FORMS += \
    widgets/loginwidget.ui \
    widgets/mainwindow.ui \
    widgets/aboutthisappdialog.ui \
    widgets/conversationitemwidget.ui \
    widgets/conversationwidget.ui \
    widgets/conversationmessagewidget.ui \
    widgets/currentuserinformationwidget.ui

RESOURCES += \
    res/ressources.qrc
