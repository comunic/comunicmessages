/**
 * Project configuration
 *
 * @author Pierre HUBERT
 */
#ifndef CONFIG_H
#define CONFIG_H

/**
 * Application information
 */
#define ORGANIZATION_NAME "Communiquons"
#define ORGANIZATION_DOMAIN "communiquons.org"
#define APPLICATION_NAME "ComunicMessages"

/**
 * API credentials
 *
 * Note : only HTTPS should be used in production
 */
#define API_URL "https://api.communiquons.org/"
#define API_SERVICE_NAME "ComunicAndroid"
#define API_SERVICE_TOKEN "cWHlmMS5A1"


/**
 * Conversations list information
 */
#define CONVERSATION_LIST_REFRESH_INTERVAL 10


/**
 * Settings information
 */
#define SETTINGS_ACCOUNT_LOGIN_TOKEN_1 "account_login_token_1"
#define SETTINGS_ACCOUNT_LOGIN_TOKEN_2 "account_login_token_2"


/**
 * Conversations information
 */
#define CONVERSATION_MESSAGE_MIN_LENGTH 3
#define CONVERSATION_MESSAGES_REFRESH_INTERVAL 1000
#define NUMBER_OF_OLDER_MESSAGES_TO_GET 10

/**
 * Images load manager information
 */
#define REMOTE_IMAGES_CACHE_DIRECTORY "remote_images"

#endif // CONFIG_H
