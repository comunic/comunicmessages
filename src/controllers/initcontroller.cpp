#include <QCoreApplication>
#include <QProgressDialog>
#include <QMessageBox>

#include "../helpers/accounthelper.h"
#include "widgets/loginwidget.h"
#include "widgets/mainwindow.h"
#include "initcontroller.h"
#include "config.h"

InitController::InitController() : QObject()
{
    mAccountHelper = new AccountHelper;
    connect(mAccountHelper, &AccountHelper::refreshCurrentUserIDResult, this, &InitController::getUserIDCallback);
}

InitController::~InitController()
{
    mAccountHelper->deleteLater();

    if(mProgressdialog != nullptr)
        mProgressdialog->deleteLater();
}

void InitController::init()
{
    //Define some basic values
    QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
    QCoreApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
    QCoreApplication::setApplicationName(APPLICATION_NAME);

    //Determine whether user is signed in or not
    if(!mAccountHelper->signedIn()) {
        LoginWidget *widget = new LoginWidget();
        widget->show();
        return;
    }

    //Display startup splash screen
    mProgressdialog = new QProgressDialog(QObject::tr("Starting up..."), QString(), 0, 3);
    mProgressdialog->show();

    //First, we need to refresh current user ID
    mProgressdialog->setLabelText(tr("Get current user ID..."));
    mProgressdialog->setValue(1);
    mAccountHelper->refreshCurrentUserID();

}

void InitController::getUserIDCallback(bool success)
{
    if(!success){
        QMessageBox::warning(mProgressdialog, tr("Error"), tr("Could not get current user ID! Please check your internet connection..."));
        deleteLater();
        return;
    }

    mProgressdialog->setLabelText(tr("Open main window..."));
    mProgressdialog->setValue(2);
    (new MainWindow())->show();

    mProgressdialog->hide();
    deleteLater();
}
