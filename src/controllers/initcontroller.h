/**
 * Initialization controller
 *
 * Used to initialize the application
 *
 * @author Pierre HUBERT
 */

#ifndef INITCONTROLLER_H
#define INITCONTROLLER_H

#include <QObject>

class QProgressDialog;
class AccountHelper;

class InitController : public QObject
{
    Q_OBJECT

public:
    InitController();
    ~InitController();

    /**
     * Initialize the application. This operation can be
     * run several times
     */
    void init();


private slots:

    void getUserIDCallback(bool success);

private:
    AccountHelper *mAccountHelper = nullptr;
    QProgressDialog *mProgressdialog = nullptr;
};

#endif // INITCONTROLLER_H
