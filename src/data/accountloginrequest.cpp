#include "accountloginrequest.h"

AccountLoginRequest::AccountLoginRequest()
{

}

QString AccountLoginRequest::emailAddress() const
{
    return mEmailAddress;
}

void AccountLoginRequest::setEmailAddress(const QString &emailAddress)
{
    mEmailAddress = emailAddress;
}

QString AccountLoginRequest::password() const
{
    return mPassword;
}

void AccountLoginRequest::setPassword(const QString &password)
{
    mPassword = password;
}
