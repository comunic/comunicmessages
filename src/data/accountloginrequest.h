/**
 * Account login request
 *
 * @author Pierre HUBERT
 */

#ifndef ACCOUNTLOGINREQUEST_H
#define ACCOUNTLOGINREQUEST_H

#include <QObject>

/**
 * Possible login results
 */
enum LoginResult {
    LOGIN_SUCCESS,
    INVALID_CREDENTIALS,
    TOO_MANY_REQUEST,
    NETWORK_ERROR,
    INVALID_SERVER_RESPONSE,
    OTHER_ERROR
};

/**
 * Login request
 */
class AccountLoginRequest
{

public:
    AccountLoginRequest();

    QString emailAddress() const;
    void setEmailAddress(const QString &emailAddress);

    QString password() const;
    void setPassword(const QString &password);

private:
    QString mEmailAddress;
    QString mPassword;
};

#endif // ACCOUNTLOGINREQUEST_H
