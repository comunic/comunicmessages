#include "accountlogintokens.h"

AccountLoginTokens::AccountLoginTokens()
{

}

bool AccountLoginTokens::isValid()
{
    return token1().length() > 0 && token2().length() > 0;
}

QString AccountLoginTokens::token1() const
{
    return mToken1;
}

void AccountLoginTokens::setToken1(const QString &token1)
{
    mToken1 = token1;
}

QString AccountLoginTokens::token2() const
{
    return mToken2;
}

void AccountLoginTokens::setToken2(const QString &token2)
{
    mToken2 = token2;
}
