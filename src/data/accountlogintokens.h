/**
 * Account Login tokens
 *
 * @author Pierre HUBERT
 */

#ifndef ACCOUNTLOGINTOKENS_H
#define ACCOUNTLOGINTOKENS_H

#include <QString>

class AccountLoginTokens
{
public:
    AccountLoginTokens();

    /**
     * Check if the tokens present in this object
     * are valid or not
     *
     * @return TRUE if the tokens are valid / FALSE else
     */
    bool isValid();

    QString token1() const;
    void setToken1(const QString &token1);

    QString token2() const;
    void setToken2(const QString &token2);

private:
    QString mToken1;
    QString mToken2;
};

#endif // ACCOUNTLOGINTOKENS_H
