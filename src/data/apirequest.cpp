#include <QNetworkReply>
#include <QFile>

#include "apirequest.h"

APIRequest::APIRequest(QObject *parent) : QObject(parent)
{

}

APIRequest::~APIRequest()
{
    if(mNetworkReply != nullptr)
        mNetworkReply->deleteLater();
}

QString APIRequest::URI() const
{
    return mURI;
}

void APIRequest::setURI(const QString &uRI)
{
    mURI = uRI;
}

void APIRequest::addString(QString name, QString value)
{
    mArguments.append(APIRequestParameter(name, value));
}

void APIRequest::addInt(QString name, int value)
{
    mArguments.append(APIRequestParameter(name, QString::number(value)));
}

void APIRequest::addBool(QString name, bool value)
{
    mArguments.append(APIRequestParameter(name, value ? "true" : "false"));
}

void APIRequest::addFileFromPath(const QString &name, const QString &path, const QString &fileType)
{
    //Determine files name for the request
    QString partName = name;
    partName.replace("\"", "\\\"");
    QString fileName = ("/"+path).split("/").last();
    fileName.replace("\"", "\\\"");

    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentTypeHeader, fileType);
    part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\""+partName+"\"; filename=\""+fileName+"\""));

    QFile *file = new QFile(path);
    if(!file->open(QIODevice::ReadOnly)){
        qWarning("Could not open file to send: %s !", path.toStdString().c_str());
        return;
    }

    part.setBodyDevice(file);
    mParts.append(part);

    //Automatically delete file object once request is completed
    file->setParent(this);
}

QList<APIRequestParameter> APIRequest::arguments() const
{
    return mArguments;
}

QNetworkReply *APIRequest::networkReply() const
{
    return mNetworkReply;
}

void APIRequest::setNetworkReply(QNetworkReply *networkReply)
{
    mNetworkReply = networkReply;
}

QList<QHttpPart> *APIRequest::parts()
{
    return &mParts;
}

bool APIRequest::hasParts() const
{
    return mParts.size() > 0;
}
