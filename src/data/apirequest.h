/**
 * This object contains information about a single
 * object.
 *
 * @author Pierre HUBERT
 */

#ifndef APIREQUEST_H
#define APIREQUEST_H

#include <QObject>
#include <QJsonDocument>
#include <QHttpPart>

#include "apirequestparameter.h"

class QNetworkReply;

class APIRequest : public QObject
{
    Q_OBJECT
public:
    explicit APIRequest(QObject *parent = nullptr);
    ~APIRequest();

    //Get and set URI target
    QString URI() const;
    void setURI(const QString &URI);

    /**
     * Add a string parameter to the request
     *
     * @param name The name of the field to add
     * @param value The value of the argument to add
     */
    void addString(QString name, QString value);

    /**
     * Add an integer parameter to the request
     *
     * @param name The name of the field to add
     * @param value The value of the argument to add
     */
    void addInt(QString name, int value);

    /**
     * Add a boolean parameter to the request
     *
     * @param name The name of the field to add
     * @param value The value of the argument to add
     */
    void addBool(QString name, bool value);

    /**
     * Add a file from filesystem path
     *
     * @param name The name of the file to add
     * @param path The path to the file
     * @param fileType The type of the file to add
     */
    void addFileFromPath(const QString &name, const QString &path, const QString &fileType);

    /**
     * Get the entire list of arguments of the request
     *
     * @return The list of arguments
     */
    QList<APIRequestParameter> arguments() const;

    /**
     * Get the list of HTTP parts included with this request
     *
     * @return Pointer on the list containing the list of parts
     */
    QList<QHttpPart> *parts();
    bool hasParts() const;

    //Get and set network reply associated with the request
    QNetworkReply *networkReply() const;
    void setNetworkReply(QNetworkReply *networkReply);

signals:

    /**
     * Signal emitted when an error occurred
     *
     * @param code The code of the error
     */
    void error(int code);

    /**
     * Signal emitted once we have got the response for our request
     *
     * @param document JSON Response
     */
    void success(const QJsonDocument &document);

    /**
     * Signal emitted once we consider the request as finished
     * It is emitted both in case of error and in case of success
     *
     * @param code The code of the error
     * @param document Data received in case of success
     */
    void finished(int code, const QJsonDocument &document);

public slots:

private:
    QString mURI;
    QList<APIRequestParameter> mArguments;
    QList<QHttpPart> mParts;
    QNetworkReply *mNetworkReply = nullptr;
};

#endif // APIREQUEST_H
