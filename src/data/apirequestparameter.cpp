#include "apirequestparameter.h"

APIRequestParameter::APIRequestParameter()
{

}

APIRequestParameter::APIRequestParameter(const QString &name, const QString &value) :
    mName(name), mValue(value)
{

}

QString APIRequestParameter::name() const
{
    return mName;
}

void APIRequestParameter::setName(const QString &name)
{
    mName = name;
}

QString APIRequestParameter::value() const
{
    return mValue;
}

void APIRequestParameter::setValue(const QString &value)
{
    mValue = value;
}
