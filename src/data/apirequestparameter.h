/**
 * Single API request parameter
 *
 * @author Pierre HUBERT
 */

#ifndef APIREQUESTPARAMETER_H
#define APIREQUESTPARAMETER_H

#include <QString>

class APIRequestParameter
{
public:
    APIRequestParameter();
    APIRequestParameter(const QString &name, const QString &value);

    QString name() const;
    void setName(const QString &name);

    QString value() const;
    void setValue(const QString &value);

private:
    QString mName;
    QString mValue;
};

#endif // APIREQUESTPARAMETER_H
