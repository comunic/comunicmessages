#include "apirequest.h"
#include "apirequestslist.h"

APIRequestsList::APIRequestsList() : QList()
{

}

APIRequest *APIRequestsList::findForReply(QNetworkReply *reply, bool delete_from_list)
{
    bool found = false;
    int i;
    for(i = 0; i < size(); i++){
        if(at(i)->networkReply() == reply){
            found = true;
            break;
        }
    }

    APIRequest *request = nullptr;

    if(found)
        request = at(i);

    if(found && delete_from_list)
        removeAt(i);

    return request;
}
