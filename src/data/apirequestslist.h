#ifndef APIREQUESTSLIST_H
#define APIREQUESTSLIST_H

#include <QList>

class APIRequest;
class QNetworkReply;

class APIRequestsList : public QList<APIRequest *>
{
public:
    APIRequestsList();

    /**
     * Search and return the API Request that contains a specific NetworkReply
     *
     * @param reply The reply to search
     * @param delete_from_list Specify whether the entry should be removed
     * from the list or not
     * @return The request / null if none found
     */
    APIRequest *findForReply(QNetworkReply *reply, bool delete_from_list);
};

#endif // APIREQUESTSLIST_H
