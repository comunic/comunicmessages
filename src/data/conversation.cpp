#include "conversation.h"
#include "userslist.h"

Conversation::Conversation()
{

}

int Conversation::iD() const
{
    return mID;
}

void Conversation::setID(int iD)
{
    mID = iD;
}

int Conversation::iDowner() const
{
    return mIDowner;
}

void Conversation::setIDowner(int iDowner)
{
    mIDowner = iDowner;
}

int Conversation::lastActive() const
{
    return mLastActive;
}

void Conversation::setLastActive(int lastActive)
{
    mLastActive = lastActive;
}

QString Conversation::name() const
{
    return mName;
}

void Conversation::setName(const QString &name)
{
    mName = name;
}

bool Conversation::following() const
{
    return mFollowing;
}

void Conversation::setFollowing(bool following)
{
    mFollowing = following;
}

bool Conversation::sawLastMessage() const
{
    return mSawLastMessage;
}

void Conversation::setSawLastMessage(bool sawLastMessage)
{
    mSawLastMessage = sawLastMessage;
}

QList<int> Conversation::members() const
{
    return mMembers;
}

void Conversation::setMembers(const QList<int> &members)
{
    mMembers = members;
}
