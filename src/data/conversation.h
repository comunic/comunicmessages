/**
 * This widget contains information about
 * a single conversation
 *
 * @author Pierre HUBERT
 */

#ifndef CONVERSATION_H
#define CONVERSATION_H

#include <QList>
#include <QString>

class Conversation
{
public:
    Conversation();

    int iD() const;
    void setID(int iD);

    int iDowner() const;
    void setIDowner(int iDowner);

    int lastActive() const;
    void setLastActive(int lastActive);

    QString name() const;
    void setName(const QString &name);

    bool following() const;
    void setFollowing(bool following);

    bool sawLastMessage() const;
    void setSawLastMessage(bool sawLastMessage);

    QList<int> members() const;
    void setMembers(const QList<int> &members);

private:
    int mID;
    int mIDowner;
    int mLastActive;
    QString mName;
    bool mFollowing;
    bool mSawLastMessage;
    QList<int> mMembers;
};

#endif // CONVERSATION_H
