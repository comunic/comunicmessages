#include "conversationmessage.h"

ConversationMessage::ConversationMessage()
{

}

int ConversationMessage::iD() const
{
    return mID;
}

void ConversationMessage::setID(int iD)
{
    mID = iD;
}

int ConversationMessage::userID() const
{
    return mUserID;
}

void ConversationMessage::setUserID(int userID)
{
    mUserID = userID;
}

int ConversationMessage::timeInsert() const
{
    return mTimeInsert;
}

void ConversationMessage::setTimeInsert(int timeInsert)
{
    mTimeInsert = timeInsert;
}

QString ConversationMessage::message() const
{
    return mMessage;
}

void ConversationMessage::setMessage(const QString &message)
{
    mMessage = message;
}

QString ConversationMessage::imagePath() const
{
    return mImagePath;
}

bool ConversationMessage::hasImage() const
{
    return !mImagePath.isEmpty();
}

void ConversationMessage::setImagePath(const QString &imagePath)
{
    mImagePath = imagePath;
}

bool ConversationMessage::operator<(const ConversationMessage &b) const
{
    return b.iD() > iD();
}
