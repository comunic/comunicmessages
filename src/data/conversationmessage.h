/**
 * Conversation message object
 *
 * @author Pierre HUBERT
 */

#ifndef CONVERSATIONMESSAGE_H
#define CONVERSATIONMESSAGE_H

#include <QString>

class ConversationMessage
{
public:
    ConversationMessage();

    int iD() const;
    void setID(int iD);

    int userID() const;
    void setUserID(int userID);

    int timeInsert() const;
    void setTimeInsert(int timeInsert);

    QString message() const;
    void setMessage(const QString &message);

    QString imagePath() const;
    bool hasImage() const;
    void setImagePath(const QString &imagePath);

    bool operator<(const ConversationMessage &b) const;

private:
    int mID;
    int mUserID;
    int mTimeInsert;
    QString mMessage;
    QString mImagePath;
};

#endif // CONVERSATIONMESSAGE_H
