#include "conversationmessageslist.h"

ConversationMessagesList::ConversationMessagesList()
{

}

int ConversationMessagesList::getOldestMessageID()
{
    if(count() == 0)
        return -1;

    return at(0).iD();
}

int ConversationMessagesList::getLastMessageID()
{

    //Return -1 by default if there is not any message in the conversation
    if(count() == 0)
        return -1;

    return at(count() -1).iD();
}
