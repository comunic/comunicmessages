/**
 * Conversations messages list container
 *
 * @author Pierre HUBERT
 */

#ifndef CONVERSATIONMESSAGESLIST_H
#define CONVERSATIONMESSAGESLIST_H

#include <QList>

#include "conversationmessage.h"

class ConversationMessagesList : public QList<ConversationMessage>
{
public:
    ConversationMessagesList();

    /**
     * Get and return the oldest message of the conversation
     *
     * @return The ID of the oldest message / -1 if none found
     */
    int getOldestMessageID();

    /**
     * Get the ID of the newest message of
     * the conversation
     *
     * @return The ID of the message / -1 in case of failure
     */
    int getLastMessageID();
};

#endif // CONVERSATIONMESSAGESLIST_H
