#include "conversationslist.h"

ConversationsList::ConversationsList()
{

}

QList<int> ConversationsList::getAllMembersId() const
{
    QList<int> members;
    for(Conversation conv : *this){
        for(int memberID : conv.members())
            if(!members.contains(memberID))
                members.append(memberID);
    }
    return members;
}

UsersList ConversationsList::getMembersInformation() const
{
    return mMembersInformation;
}

void ConversationsList::setMembersInformation(const UsersList &membersInformation)
{
    mMembersInformation = membersInformation;
}
