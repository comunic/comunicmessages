/**
 * Conversations list
 *
 * @author Pierre HUBERT
 */

#ifndef CONVERSATIONSLIST_H
#define CONVERSATIONSLIST_H

#include <QList>

#include "conversation.h"
#include "user.h"
#include "userslist.h"

class ConversationsList : public QList<Conversation>
{
public:
    ConversationsList();

    /**
     * Get and return the ID of all the members of
     * the conversations
     *
     * @return The IDs of the conversations
     */
    QList<int> getAllMembersId() const;

    UsersList getMembersInformation() const;
    void setMembersInformation(const UsersList &membersInformation);

private:

    //Private fields
    UsersList mMembersInformation;
};

#endif // CONVERSATIONSLIST_H
