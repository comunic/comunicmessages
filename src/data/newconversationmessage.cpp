#include "newconversationmessage.h"

NewConversationMessage::NewConversationMessage()
{

}

int NewConversationMessage::iDConversation() const
{
    return mIDConversation;
}

void NewConversationMessage::setIDConversation(int iDConversation)
{
    mIDConversation = iDConversation;
}

QString NewConversationMessage::message() const
{
    return mMessage;
}

void NewConversationMessage::setMessage(const QString &message)
{
    mMessage = message;
}

QString NewConversationMessage::imagePath() const
{
    return mImagePath;
}

bool NewConversationMessage::hasImage() const
{
    return !mImagePath.isEmpty();
}

void NewConversationMessage::setImagePath(const QString &imagePath)
{
    mImagePath = imagePath;
}
