/**
 * New conversation message
 *
 * Contains information about a message to send
 * to the server
 *
 * @author Pierre HUBERT
 */
#ifndef NEWCONVERSATIONMESSAGE_H
#define NEWCONVERSATIONMESSAGE_H

#include <QString>

class NewConversationMessage
{
public:
    NewConversationMessage();

    int iDConversation() const;
    void setIDConversation(int iDConversation);

    QString message() const;
    void setMessage(const QString &message);

    QString imagePath() const;
    bool hasImage() const;
    void setImagePath(const QString &imagePath);

private:
    int mIDConversation;
    QString mMessage;
    QString mImagePath;
};

#endif // NEWCONVERSATIONMESSAGE_H
