/**
 * QLabelHolder
 *
 * This file should be referenced by ImageLoadHelper ONLY !!!
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QObject>
#include <QLabel>

/**
 * This class is used to avoid memory leak if attempting to
 * apply downloaded image to a deleted label
 */
class QLabelHolder : public QObject
{
    Q_OBJECT
public:
    QLabelHolder(QLabel *label) {
        mLabel = label;
        connect(label, &QLabel::destroyed, this, &QLabelHolder::deleted);
    }

    QLabel *label(){
        return mLabel;
    }

private slots:

    void deleted(){
        mLabel = nullptr;
    }

private:

    //Private fields
    QLabel *mLabel;
};
