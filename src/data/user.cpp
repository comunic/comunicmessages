#include "user.h"

User::User()
{

}

int User::iD() const
{
    return mID;
}

void User::setID(int iD)
{
    mID = iD;
}

QString User::firstName() const
{
    return mFirstName;
}

void User::setFirstName(const QString &firstName)
{
    mFirstName = firstName;
}

QString User::lastName() const
{
    return mLastName;
}

void User::setLastName(const QString &lastName)
{
    mLastName = lastName;
}

QString User::displayName() const
{
    return firstName() + " " + lastName();
}

QString User::accountImage() const
{
    return mAccountImage;
}

void User::setAccountImage(const QString &accountImage)
{
    mAccountImage = accountImage;
}
