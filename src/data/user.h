/**
 * This object contains information about a single user
 *
 * @author Pierre HUBERT
 */

#ifndef USER_H
#define USER_H

#include <QString>

class User
{
public:
    User();

    int iD() const;
    void setID(int iD);

    QString firstName() const;
    void setFirstName(const QString &firstName);

    QString lastName() const;
    void setLastName(const QString &lastName);

    /**
     * Get and return display name of the user
     */
    QString displayName() const;

    QString accountImage() const;
    void setAccountImage(const QString &accountImage);

private:
    int mID;
    QString mFirstName;
    QString mLastName;
    QString mAccountImage;
};

#endif // USER_H
