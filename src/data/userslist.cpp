#include "userslist.h"

UsersList::UsersList() : QList()
{

}

User UsersList::get(int userID) const
{
    for(User user : *this)
        if(user.iD() == userID)
            return user;

    //User not found
    return User();
}
