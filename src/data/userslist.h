/**
 * A list of users
 *
 * Provides methods to fetch users
 *
 * @author Pierre HUBERT
 */

#ifndef USERSLISTS_H
#define USERSLISTS_H

#include <QList>

#include "user.h"

class UsersList : public QList<User>
{
public:
    UsersList();

    /**
     * Find and return information about a user specified
     * by its ID
     *
     * @param userID The ID of the target user
     * @return Information about the user
     */
    User get(int userID) const;
};

#endif // USERSLISTS_H
