#include <QJsonObject>

#include "accounthelper.h"
#include "configurationhelper.h"
#include "../data/apirequest.h"
#include "../data/accountlogintokens.h"
#include "../helpers/apihelper.h"

//Current user ID
static int mUserID = -1;

AccountHelper::AccountHelper(QObject *parent) : QObject(parent)
{
    mAPIHelper = new APIHelper;
}

bool AccountHelper::signedIn()
{
    return ConfigurationHelper().getAccountTokens().isValid();
}

void AccountHelper::logout()
{
    //Destroy login tokens
    ConfigurationHelper().setAccountTokens(AccountLoginTokens());
}

void AccountHelper::login(const AccountLoginRequest &info)
{
    //Create API request
    APIRequest *request = new APIRequest;
    request->setURI("account/login");
    request->addString("userMail", info.emailAddress());
    request->addString("userPassword", info.password());

    //Make connections
    connect(request, &APIRequest::success, this, &AccountHelper::requestLoginResult);
    connect(request, &APIRequest::error, this, &AccountHelper::loginError);

    //Execute request
    mAPIHelper->execute(request);

}

void AccountHelper::refreshCurrentUserID()
{
    APIRequest *request = new APIRequest;
    request->setURI("user/getCurrentUserID");
    connect(request, &APIRequest::error, this, &AccountHelper::getUserIdCallbackError);
    connect(request, &APIRequest::success, this, &AccountHelper::getUserIdCallbackSuccess);
    mAPIHelper->execute(request);
}

void AccountHelper::loginError(int code)
{
    //Delete API request
    qobject_cast<APIRequest *>(sender())->deleteLater();

    LoginResult result = LoginResult::OTHER_ERROR;

    if(code < 100)
        result = LoginResult::NETWORK_ERROR;
    if(code == 401)
        result = LoginResult::INVALID_CREDENTIALS;
    if(code == 429)
        result = LoginResult::TOO_MANY_REQUEST;

    //Inform about the result
    emit loginResult(result);
}

void AccountHelper::requestLoginResult(const QJsonDocument &document)
{
    //Delete API request
    qobject_cast<APIRequest *>(sender())->deleteLater();

    //Intend to parse server response
    QJsonObject object = document.object().value("tokens").toObject();

    if(object.isEmpty()){
        qWarning("objects 'tokens' of server response is empty!");
        emit loginResult(LoginResult::INVALID_SERVER_RESPONSE);
        return;
    }

    //Intend to parse get login responses
    AccountLoginTokens tokens;
    tokens.setToken1(object.value("token1").toString());
    tokens.setToken2(object.value("token2").toString());
    ConfigurationHelper().setAccountTokens(tokens);

    //Success
    emit loginResult(LoginResult::LOGIN_SUCCESS);
}

void AccountHelper::getUserIdCallbackError()
{
    qobject_cast<APIRequest *>(sender())->deleteLater();

    emit refreshCurrentUserIDResult(false);
}

void AccountHelper::getUserIdCallbackSuccess(const QJsonDocument &document)
{
    qobject_cast<APIRequest *>(sender())->deleteLater();

    int userID = document.object().value("userID").toInt(-1);

    if(userID < 0){
        qWarning("Could not get user ID!");
        emit refreshCurrentUserIDResult(false);
        return;
    }

    mUserID = userID;

    emit refreshCurrentUserIDResult(true);
}

int AccountHelper::getUserID()
{
    if(mUserID < 1)
        qWarning("An attempt to get user ID has been made while user ID has not been refreshed yet!");

    return mUserID;
}
