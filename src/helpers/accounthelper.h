/**
 * Account helper
 *
 * @author Pierre HUBERT
 */

#ifndef ACCOUNTHELPER_H
#define ACCOUNTHELPER_H

#include <QObject>
#include <QJsonDocument>

class APIHelper;

#include "../data/accountloginrequest.h"

class AccountHelper : public QObject
{
    Q_OBJECT
public:
    explicit AccountHelper(QObject *parent = nullptr);

    /**
     * Determine wether user is signed in or not
     *
     * @return TRUE if signed in / FALSE else
     */
    bool signedIn();

    /**
     * Logout user
     */
    void logout();

    /**
     * Perform login
     *
     * @param info Request data (user credentials)
     */
    void login(const AccountLoginRequest &info);

    /**
     * Refresh current user ID
     */
    void refreshCurrentUserID();

    /**
     * Get current cached user ID
     *
     * @return The ID of the user ID / note : invalid value if user ID
     * is not available yet
     */
    static int getUserID();

signals:

    /**
     * This signal is emitted once a login request has been completed
     *
     * @param result The state of the request
     */
    void loginResult(LoginResult result);

    /**
     * Refresh current user ID result
     *
     * @param success TRUE in case of success / FALSE else
     */
    void refreshCurrentUserIDResult(bool success);

public slots:

private slots:

    //Login request callbacks
    void loginError(int code);
    void requestLoginResult(const QJsonDocument &document);

    //Get current user id callbacks
    void getUserIdCallbackError();
    void getUserIdCallbackSuccess(const QJsonDocument &document);

private:
    APIHelper *mAPIHelper;
};

#endif // ACCOUNTHELPER_H
