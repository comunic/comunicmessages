#include <QUrlQuery>
#include <QJsonDocument>

#include "apihelper.h"
#include "accounthelper.h"
#include "configurationhelper.h"
#include "../data/accountlogintokens.h"
#include "../config.h"

APIHelper::APIHelper(QObject *parent) : QObject(parent)
{

    //Make errors
    QObject::connect(&mNetworkManager, &QNetworkAccessManager::sslErrors, this, &APIHelper::sslErrors);
}

void APIHelper::execute(APIRequest *request)
{
    //Determine full request URL
    QString requestURL = API_URL + request->URI();

    //Add API credentials to parameters
    request->addString("serviceName", API_SERVICE_NAME);
    request->addString("serviceToken", API_SERVICE_TOKEN);

    //Add account tokens if available
    if(AccountHelper().signedIn()){
        AccountLoginTokens tokens = ConfigurationHelper().getAccountTokens();
        request->addString("userToken1", tokens.token1());
        request->addString("userToken2", tokens.token2());
    }

    QNetworkReply *reply = nullptr;

    //Prepare request
    //Check if the request contains files or not
    if(!request->hasParts()){

        //See this SO question to learn more : https://stackoverflow.com/questions/2599423
        QUrlQuery queryData;
        for(APIRequestParameter param : request->arguments())
            queryData.addQueryItem(param.name(), param.value());

        //Send request
        QNetworkRequest networkRequest((QUrl(requestURL)));
        networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        reply = mNetworkManager.post(networkRequest, queryData.toString(QUrl::FullyEncoded).toUtf8());

    }

    //Multiple entries request
    else {

        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

        //Process the list of "normal" arguments
        for(APIRequestParameter param : request->arguments()){
            QHttpPart part;
            part.setHeader(QNetworkRequest::ContentDispositionHeader, "form-data; name=\""+param.name()+"\"");
            part.setBody(param.value().toStdString().c_str());
            multiPart->append(part);
        }

        //Append all the other parts
        for(int i = 0; i < request->parts()->size(); i++)
            multiPart->append(request->parts()->at(i));

        //Send request
        QNetworkRequest networkRequest((QUrl(requestURL)));
        reply = mNetworkManager.post(networkRequest, multiPart);

        //Delete multipart as soon as the request ends
        multiPart->setParent(reply);

    }

    //Make connections
    connect(reply, &QNetworkReply::finished, this, &APIHelper::finished);
    connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &APIHelper::error);

    //Add the request to the list
    request->setNetworkReply(reply);
    mRequestsList.append(request);
}

void APIHelper::finished()
{
    if(qobject_cast<QNetworkReply *>(sender())->attribute(QNetworkRequest::HttpStatusCodeAttribute) != 200)
        return;

    //Get the API request related to this
    APIRequest *request = mRequestsList.findForReply(qobject_cast<QNetworkReply *>(sender()), true);

    //Process and return response
    QJsonDocument document = QJsonDocument::fromJson(request->networkReply()->readAll());
    emit request->success(document);
    emit request->finished(200, document);
}

void APIHelper::error()
{
    //Get the API request related to this
    APIRequest *request = mRequestsList.findForReply(qobject_cast<QNetworkReply *>(sender()), true);

    //Try to determine error code
    int response_code = request->networkReply()->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    qWarning("An error occurred in an API request! (code: %d)", response_code);

    emit request->error(response_code);
    emit request->finished(response_code, QJsonDocument());
}

void APIHelper::sslErrors()
{
    qWarning("APIHelper: the SSL/TLS session encountered an error!");
}
