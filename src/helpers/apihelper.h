/**
 * API Helper - handles the connections between
 * the server and this application
 *
 * @author Pierre HUBERT
 */

#ifndef APIHELPER_H
#define APIHELPER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "../data/apirequest.h"
#include "../data/apirequestslist.h"

class APIHelper : public QObject
{
    Q_OBJECT
public:
    explicit APIHelper(QObject *parent = nullptr);

    /**
     * Send a request to the server
     *
     * @param request The request to execute
     */
    void execute(APIRequest *request);

signals:

public slots:

private slots:
    void finished();
    void error();
    void sslErrors();

private:
    QNetworkAccessManager mNetworkManager;
    APIRequestsList mRequestsList;
};

#endif // APIHELPER_H
