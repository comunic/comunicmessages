#include <QSettings>

#include "configurationhelper.h"
#include "../config.h"

ConfigurationHelper::ConfigurationHelper()
{
    mSettings = new QSettings();
}

ConfigurationHelper::~ConfigurationHelper()
{
    mSettings->deleteLater();
}

AccountLoginTokens ConfigurationHelper::getAccountTokens()
{
    //Check if we have both of the tokens
    if(!mSettings->value(SETTINGS_ACCOUNT_LOGIN_TOKEN_1).isValid() ||
            !mSettings->value(SETTINGS_ACCOUNT_LOGIN_TOKEN_1).isValid())
        return AccountLoginTokens(); //Return invalid object : not all settings available

    //Parse and return account login tokens
    AccountLoginTokens tokens;
    tokens.setToken1(mSettings->value(SETTINGS_ACCOUNT_LOGIN_TOKEN_1).toString());
    tokens.setToken2(mSettings->value(SETTINGS_ACCOUNT_LOGIN_TOKEN_2).toString());
    return tokens;
}

void ConfigurationHelper::setAccountTokens(const AccountLoginTokens &tokens)
{
    mSettings->setValue(SETTINGS_ACCOUNT_LOGIN_TOKEN_1, tokens.token1());
    mSettings->setValue(SETTINGS_ACCOUNT_LOGIN_TOKEN_2, tokens.token2());
}
