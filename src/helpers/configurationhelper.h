/**
 * Configuration helper
 *
 * @author Pierre HUBERT
 */

#ifndef CONFIGURATIONHELPER_H
#define CONFIGURATIONHELPER_H

#include <QObject>

#include "../data/accountlogintokens.h"

class QSettings;

class ConfigurationHelper
{
public:
    ConfigurationHelper();
    ~ConfigurationHelper();

    /**
     * Get the login tokens associated with this account
     *
     * @return The login tokens associated with this account /
     * invalid object if none found
     */
    AccountLoginTokens getAccountTokens();

    /**
     * Set the login tokens associated with the current account
     *
     * @param tokens The tokens to set
     */
    void setAccountTokens(const AccountLoginTokens &tokens);

private:
    QSettings *mSettings;
};

#endif // CONFIGURATIONHELPER_H
