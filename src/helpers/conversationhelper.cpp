#include <QJsonObject>
#include <QJsonArray>

#include "apihelper.h"
#include "conversationhelper.h"
#include "../data/apirequest.h"
#include "../utils/filesutils.h"
#include "../config.h"

ConversationHelper::ConversationHelper(QObject *parent) : QObject(parent)
{
    mAPIHelper = new APIHelper(this);
}

void ConversationHelper::sendMessage(const NewConversationMessage &message)
{
    APIRequest *request = new APIRequest;
    request->setURI("conversations/sendMessage");
    request->addInt("conversationID", message.iDConversation());
    request->addString("message", message.message());

    //Add image (if any)
    if(message.hasImage()){

        //Add image to request
        request->addFileFromPath("image", message.imagePath(), FilesUtils::GetFileMimeType(message.imagePath()));

    }

    connect(request, &APIRequest::finished, this, &ConversationHelper::sendMessageFinished);
    mAPIHelper->execute(request);
}

void ConversationHelper::getMessages(int conversationID, int last_message_id)
{
    APIRequest *request = new APIRequest;
    request->setURI("conversations/refresh_single");
    request->addInt("conversationID", conversationID);
    request->addInt("last_message_id", last_message_id > 0 ? last_message_id : 0);

    connect(request, &APIRequest::finished, this, &ConversationHelper::getMessagesFinished);
    mAPIHelper->execute(request);
}

void ConversationHelper::getOlderMessages(int conversationID, int oldest_message_id)
{
    APIRequest *request = new APIRequest;
    request->setURI("conversations/get_older_messages");
    request->addInt("conversationID", conversationID);
    request->addInt("oldest_message_id", oldest_message_id);
    request->addInt("limit", NUMBER_OF_OLDER_MESSAGES_TO_GET);

    connect(request, &APIRequest::finished, this, &ConversationHelper::getMessagesFinished);
    mAPIHelper->execute(request);
}

void ConversationHelper::sendMessageFinished(int code)
{
    //Delete sender
    qobject_cast<APIRequest *>(sender())->deleteLater();

    if(code != 200){
        emit sendMessageCallback(false);
        return;
    }

    //Success
    emit sendMessageCallback(true);
}

void ConversationHelper::getMessagesFinished(int code, const QJsonDocument &document)
{
    //Delete sender
    qobject_cast<APIRequest *>(sender())->deleteLater();

    //Check if the operation is not a success
    if(code != 200){
        emit getMessagesCallback(false, QList<ConversationMessage>());
        return;
    }

    QJsonArray array = document.array();
    QList<ConversationMessage> list;
    for(QJsonValue entry : array)
        list.append(QJsonObjectToConversationMessage(entry.toObject()));

    emit getMessagesCallback(true, list);
}

ConversationMessage ConversationHelper::QJsonObjectToConversationMessage(const QJsonObject &object)
{
    ConversationMessage message;
    message.setID(object.value("ID").toInt());
    message.setUserID(object.value("ID_user").toInt());
    message.setTimeInsert(object.value("time_insert").toInt());
    message.setMessage(object.value("message").toString());
    message.setImagePath(object.value("image_path").toString());
    return message;
}
