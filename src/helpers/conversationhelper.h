/**
 * Conversation helper
 *
 * This class is an interface between the server
 * and the conversations widgets
 *
 * @author Pierre HUBERT
 */
#ifndef CONVERSATIONHELPER_H
#define CONVERSATIONHELPER_H

#include <QObject>

#include "../data/newconversationmessage.h"
#include "../data/apirequest.h"
#include "../data/conversationmessage.h"

class QJsonObject;

class APIHelper;

class ConversationHelper : public QObject
{
    Q_OBJECT
public:
    explicit ConversationHelper(QObject *parent = nullptr);

    /**
     * Send a message to the server
     *
     * @param message Information about the message to send
     */
    void sendMessage(const NewConversationMessage &message);

    /**
     * Get some messages of a conversation
     *
     * @param conversationID The ID of the target conversation
     * @param last_message_id The ID of the last known message (-1 for none)
     */
    void getMessages(int conversationID, int last_message_id = -1);

    /**
     * Get older messages of a conversation
     *
     * @param conversationID The ID of the target conversation
     * @param oldest_message_id The ID of the last known message
     */
    void getOlderMessages(int conversationID, int oldest_message_id);

signals:

    /**
     * This signal is emitted when we have got the result of a create message
     * attempt
     *
     * @param success TRUE if the operation is a success / FALSE
     */
    void sendMessageCallback(bool success);

    /**
     * Get the list of messages callback
     *
     * @param success TRUE for a success / FALSE else
     * @param list The list of messages
     */
    void getMessagesCallback(bool success, QList<ConversationMessage> list);

public slots:


private slots:

    /**
     * Send message callback
     *
     * @param code The code of the error
     * @param & Server response (useless in this case)
     */
    void sendMessageFinished(int code);

    /**
     * Finished to retrieve the list of messages of the user
     *
     * @param code HTTP response code
     * @param document Server response
     */
    void getMessagesFinished(int code, const QJsonDocument &document);

private:

    /**
     * Turn a json object into a conversation object
     *
     * @param object The object to convert
     * @return Generated object
     */
    static ConversationMessage QJsonObjectToConversationMessage(const QJsonObject &object);

    //Private fields
    APIHelper *mAPIHelper;
};

#endif // CONVERSATIONHELPER_H
