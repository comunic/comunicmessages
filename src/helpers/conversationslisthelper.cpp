#include <QJsonArray>
#include <QJsonObject>

#include "conversationslisthelper.h"
#include "apihelper.h"
#include "accounthelper.h"
#include "../data/apirequest.h"
#include "../data/conversationslist.h"

ConversationsListHelper::ConversationsListHelper(QObject *parent) : QObject(parent)
{
    mAPIHelper = new APIHelper(this);
}

void ConversationsListHelper::getList()
{
    //We need to perform a request on the server
    APIRequest *request = new APIRequest(this);
    request->setURI("conversations/getList");
    connect(request, &APIRequest::finished, this, &ConversationsListHelper::getConvListCallback);
    mAPIHelper->execute(request);
}

QString ConversationsListHelper::getConversationDisplayName(const Conversation &conv, const UsersList &usersInfo)
{
    //Check if the conversation has already a name
    if(conv.name().length() > 0)
        return conv.name();

    QString name;

    int i = 0;
    for(int j = 0; j < conv.members().length() && i < 3; j++){

        //We bypass the current user name
        if(conv.members().at(j) == AccountHelper::getUserID())
            continue;

        if(name.length() > 0)
            name += ", ";
        name += usersInfo.get(conv.members().at(j)).displayName();

        i++;
    }

    return name;
}

void ConversationsListHelper::getConvListCallback(int code, const QJsonDocument &document)
{
    //Delete the request
    qobject_cast<APIRequest *>(sender())->deleteLater();

    if(code != 200){
        emit onGotList(false, ConversationsList());
        return;
    }

    //The request successfully completed
    QJsonArray arr = document.array();

    //Process the list of conversations
    ConversationsList list;
    for(int i = 0; i < arr.size(); i++)
        list.append(GetConversationFromJson(arr.at(i).toObject()));

    //Finished
    emit onGotList(true, list);
}

Conversation ConversationsListHelper::GetConversationFromJson(const QJsonObject &obj)
{
    Conversation conv;
    conv.setID(obj.value("ID").toInt());
    conv.setIDowner(obj.value("ID_owner").toInt());
    conv.setLastActive(obj.value("last_active").toInt());
    conv.setName(obj.value("name").toString());
    conv.setFollowing(obj.value("following").toInt() == 1);
    conv.setSawLastMessage(obj.value("saw_last_message").toInt() == 1);

    //Process the list of members of the conversation
    QJsonArray members_arr = obj.value("members").toArray();
    QList<int> members_list;
    for(int i = 0; i < members_arr.size(); i++){
        members_list.append(members_arr.at(i).toString().toInt());
    }
    conv.setMembers(members_list);

    return conv;
}
