/**
 * Conversations list helper
 *
 * Does all the tasks related with conversations managment
 *
 * @author Pierre HUBERT
 */

#ifndef CONVERSATIONSLISTHELPER_H
#define CONVERSATIONSLISTHELPER_H

#include <QObject>

#include "../data/conversation.h"
#include "../data/userslist.h"

class QJsonObject;

class APIHelper;
class ConversationsList;

class ConversationsListHelper : public QObject
{
    Q_OBJECT
public:
    explicit ConversationsListHelper(QObject *parent = nullptr);

    /**
     * Get the list of conversations
     */
    void getList();

    /**
     * Get and return the display name of a conversation
     *
     * @param conv Information about the target conversation
     * @param usersInfo Information about related users
     * @return The name of the conversation
     */
    static QString getConversationDisplayName(const Conversation &conv, const UsersList &usersInfo);

signals:

    /**
     * This signal is emitted once we have a got a new list of conversation
     *
     * @param success TRUE for a success / FALSE else
     * @param list The list of conversations
     */
    void onGotList(bool success, const ConversationsList &list);

public slots:

private slots:

    /**
     * This slot is triggered once the request to the server was
     * completed
     *
     * @param code Response code from the server
     * @param document Response document
     */
    void getConvListCallback(int code, const QJsonDocument &document);

private:

    /**
     * Turn a QJsonObject into a Conversation one
     *
     * @param obj The object to convert
     * @return Generated conversation object
     */
    static Conversation GetConversationFromJson(const QJsonObject &obj);

    //Private fields
    APIHelper *mAPIHelper;
};

#endif // CONVERSATIONSLISTHELPER_H
