#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QCryptographicHash>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QImage>
#include <QImageReader>
#include <QPixmap>
#include <QLabel>

#include "imageloadhelper.h"
#include "../config.h"
#include "../utils/filesutils.h"


//This header should be called only from this file
// (QObject subclass seems not to be declarable in CPP files)
#include "../data/qlabelholder.h"

/**
 * The list of pending labels for an image
 */
static QMap<QString, QList<QLabelHolder*>> privateList;

/**
 * Network Access Manager used to download images
 */
static QNetworkAccessManager accessManager;

/**
 * The first and the last instance of this object
 */
static ImageLoadHelper *mHelper = nullptr;

ImageLoadHelper::ImageLoadHelper(QObject *parent) : QObject(parent)
{
    //Nothing yet
}

void ImageLoadHelper::Load(QLabel *label, const QString &url)
{
    //Construct object if required
    if(mHelper == nullptr)
        mHelper = new ImageLoadHelper();

    //Check if the image has already been downloaded
    if(IsDownloaded(url)){
        ApplyImage(label, url);
        return;
    }

    //Check if download is already running
    if(privateList.contains(url)){
        QList<QLabelHolder *> swapList = privateList.take(url);
        swapList.append(new QLabelHolder(label));
        privateList.insert(url, swapList);
        return;
    }

    //If we get there, we have to launch the download of the image
    QList<QLabelHolder *> list;
    list.append(new QLabelHolder(label));
    privateList.insert(url, list);

    Download(url);
}

void ImageLoadHelper::NetworkError()
{
    qWarning("Image Load Helper error : Network error while connecting to server!");
}

void ImageLoadHelper::NetworkRequestFinished()
{
    //If the request is a success, we can save the image, else we can not do anything
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    reply->deleteLater();

    if(reply->error() != QNetworkReply::NoError){
        qWarning("Can not process image because we encountered an error while downloading it!");
        return;
    }

    //Save the image
    QString url = reply->url().toString();
    QImageReader reader(reply);
    QImage image = reader.read();

    //Check if the image could not be decode
    if(image.isNull()){
        qWarning("Downloaded image from %s is not valid!", url.toStdString().c_str());
        return;
    }

    //Save the image
    QString file_path = GetImageStoragePath(url);
    if(!image.save(file_path, "PNG")){
        qWarning("Could not save image from %s to %s !", url.toStdString().c_str(), file_path.toStdString().c_str());
        return;
    }

    //Process the list of awaiting labels
    QPixmap pixmap = QPixmap::fromImage(image);
    QList<QLabelHolder *> labels = privateList.take(url);
    for(QLabelHolder *currLabel : labels)
        if(currLabel->label() != nullptr)
            ApplyImage(currLabel->label(), pixmap);
}

void ImageLoadHelper::SSLErrors()
{
    qWarning("Image Load Helper error : SSL error while connecting to server!");
}

void ImageLoadHelper::Download(const QString &url)
{
    qWarning("Download image located at URL: %s", url.toStdString().c_str());

    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setRawHeader("User-Agent", "ComunicMessages Images Downloader 1.0");

    QNetworkReply *reply = accessManager.get(request);
    connect(reply, &QNetworkReply::finished, mHelper, &ImageLoadHelper::NetworkRequestFinished);
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), mHelper, SLOT(NetworkError()));
    connect(reply, &QNetworkReply::sslErrors, mHelper, &ImageLoadHelper::SSLErrors);
}

void ImageLoadHelper::ApplyImage(QLabel *label, const QString &url)
{
    ApplyImage(label, QPixmap(GetImageStoragePath(url)));
}

void ImageLoadHelper::ApplyImage(QLabel *label, const QPixmap &pixmap)
{
    //Check if we need to scale image
    if(
            (label->maximumWidth() > 100000 || label->maximumHeight() > 100000) || //No maximum size
            (label->maximumWidth() > pixmap.width() && label->maximumHeight() > pixmap.height()) //Image smaller than maximum size
    ){
        label->setPixmap(pixmap);
        return;
    }

    //Else we scale image
    label->setPixmap(pixmap.scaled(label->maximumSize(), Qt::KeepAspectRatio));
}

bool ImageLoadHelper::IsDownloaded(const QString &url)
{
    return QFile(GetImageStoragePath(url)).exists();
}

QString ImageLoadHelper::GetImageStoragePath(const QString &url)
{
    //Containing directory
    QString path = QStandardPaths::writableLocation(QStandardPaths::CacheLocation)
            + QDir::separator() + REMOTE_IMAGES_CACHE_DIRECTORY;

    //Check the directory exists. If not, create it
    if(!FilesUtils::CreateDirectoryIfNotExists(path))
        qFatal("Could not create images cache directory.");

    //Add separator to path
    path += QDir::separator();

    //File name
    path += QString(QCryptographicHash::hash(url.toStdString().c_str(), QCryptographicHash::Md5).toHex());

    return path;
}
