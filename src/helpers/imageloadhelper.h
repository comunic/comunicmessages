/**
 * ImageLoadHelper - This helper is used to
 * easily load remote images and caches them
 *
 * @author Pierre HUBERT
 */

#ifndef IMAGELOADHELPER_H
#define IMAGELOADHELPER_H

#include <QObject>

class QLabel;

class ImageLoadHelper : public QObject
{
    Q_OBJECT
public:

    /**
     * Load an image into a label
     *
     * @param label The label where the image will be applied
     * @param url The URL of the target image
     */
    static void Load(QLabel *label, const QString &url);

signals:

private slots:

    /**
     * Slot called in case of SSL error
     */
    void SSLErrors();

    /**
     * Slot called in case of network error
     */
    void NetworkError();

    /**
     * Slot called when a network request finished
     */
    void NetworkRequestFinished();

private:
    //This constructor must be private
    explicit ImageLoadHelper(QObject *parent = nullptr);

    /**
     * Download an image
     *
     * @param url The URL of the image to download
     */
    static void Download(const QString &url);

    /**
     * Apply an image to a label
     *
     * @param label Target label which will receive the image
     * @param url The remote URL of the image
     */
    static void ApplyImage(QLabel *label, const QString &url);

    /**
     * Apply an image to a label
     *
     * @param label Target label which will receive the image
     * @param pixamp The pixmap to apply to the label
     */
    static void ApplyImage(QLabel *label, const QPixmap &pixmap);

    /**
     * Check out whether an image has been already downloaded
     *
     * @param url The URL of the target image
     * @return TRUE for a success / FALSE else
     */
    static bool IsDownloaded(const QString &url);

    /**
     * Get the storage path of a remote image
     *
     * @param url The URL of the remote image
     * @return Full path to the image
     */
    static QString GetImageStoragePath(const QString &url);
};

#endif // IMAGELOADHELPER_H
