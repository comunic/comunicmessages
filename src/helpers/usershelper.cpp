#include <QJsonObject>

#include "usershelper.h"
#include "apihelper.h"

UsersHelper::UsersHelper(QObject *parent) : QObject(parent)
{
    mAPIHelper = new APIHelper(this);
}

void UsersHelper::getList(QList<int> ids)
{
    APIRequest *request = new APIRequest(this);
    request->setURI("user/getInfosMultiple");

    QString ids_str;
    for(int id : ids)
        ids_str += QString::number(id) + ",";

    request->addString("usersID", ids_str);
    mAPIHelper->execute(request);

    //Make connection
    connect(request, &APIRequest::finished, this, &UsersHelper::getUsersInformationFinished);
}

void UsersHelper::getUsersInformationFinished(int code, const QJsonDocument &document)
{
    //Delete the request
    qobject_cast<APIRequest *>(sender())->deleteLater();

    //Check for error
    if(code != 200){
        emit onGotUsersInfo(false, UsersList());
        return;
    }

    //Parse the list of object
    UsersList list;
    QJsonObject obj = document.object();
    for(QString id : obj.keys())
        list.append(ParseJSONToUser(obj.value(id).toObject()));

    emit onGotUsersInfo(true, list);
}

User UsersHelper::ParseJSONToUser(const QJsonObject &obj)
{
    User user;
    user.setID(obj.value("userID").toInt());
    user.setFirstName(obj.value("firstName").toString());
    user.setLastName(obj.value("lastName").toString());
    user.setAccountImage(obj.value("accountImage").toString());
    return user;
}
