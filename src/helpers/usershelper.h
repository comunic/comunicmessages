/**
 * Users helper
 *
 * It is used to get information about users
 *
 * @author Pierre HUBERT
 */

#ifndef USERSHELPER_H
#define USERSHELPER_H

#include <QObject>

#include "../data/user.h"
#include "../data/userslist.h"

class QJsonObject;

class APIHelper;

class UsersHelper : public QObject
{
    Q_OBJECT
public:
    explicit UsersHelper(QObject *parent = nullptr);

    /**
     * Query information about a list of users
     *
     * @param ids The ID of the users to get
     */
    void getList(QList<int> ids);

signals:

    /**
     * Signal emitted when we have got new users information
     *
     * @param success TRUE for a success / FALSE else
     * @param list Information about the users
     */
    void onGotUsersInfo(bool success, const UsersList &list);

public slots:

private slots:

    /**
     * Slot called once the API request to get users information has been finished
     *
     * @param code Result code of the operation (200 for a success)
     * @param document Document
     */
    void getUsersInformationFinished(int code, const QJsonDocument &document);

private:

    /**
     * Turn a JSON object into a User object
     *
     * @param obj The object to convert
     * @return Generated user object
     */
    User ParseJSONToUser(const QJsonObject &obj);

    //Private fields
    APIHelper *mAPIHelper;
};

#endif // USERSHELPER_H
