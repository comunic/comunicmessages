/**
 * @author Pierre HUBERT
 */

#include <QApplication>

#include "controllers/initcontroller.h"

int main(int argc, char** argv){
    QApplication app(argc, argv);

    //Initialize app
    (new InitController())->init();

    return app.exec();
}
