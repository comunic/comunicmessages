#include <QRegularExpression>

#include "accountutils.h"

AccountUtils::AccountUtils()
{

}

bool AccountUtils::CheckEmailAddress(const QString &email)
{
    QRegExp regex("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    regex.setCaseSensitivity(Qt::CaseInsensitive);
    regex.setPatternSyntax(QRegExp::RegExp);
    return regex.exactMatch(email);
}
