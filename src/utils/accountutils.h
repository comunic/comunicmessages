/**
 * Account utilities
 *
 * @author Pierre HUBERT
 */

#ifndef ACCOUNTUTILS_H
#define ACCOUNTUTILS_H

#include <QObject>

class AccountUtils
{
public:
    AccountUtils();

    /**
     * Determine whether an email address is valid or not
     *
     * @param email The email address to check
     * @return TRUE if the email is valid / FALSE else
     */
    bool static CheckEmailAddress(const QString &email);
};

#endif // ACCOUNTUTILS_H
