#include <QDir>
#include <QMimeDatabase>

#include "filesutils.h"

FilesUtils::FilesUtils()
{

}

bool FilesUtils::CreateDirectoryIfNotExists(const QString &path)
{
    QDir dir(path);

    //Check if the directory already exists
    if(dir.exists())
            return true;

    return dir.mkpath(".");
}

QString FilesUtils::GetFileMimeType(const QString &filePath)
{
    return (QMimeDatabase()).mimeTypeForFile(filePath).name();
}
