/**
 * Files utilities
 *
 * @author Pierre HUBERT
 */

#ifndef FILESUTILS_H
#define FILESUTILS_H

#include <QString>

class FilesUtils
{
public:
    FilesUtils();

    /**
     * Create the specified directory and all its parents if they do not
     * exists
     *
     * @param path The path of the folder to check
     * @return TRUE if the folder exists or has been successfully created /
     *          FALSE else
     */
    static bool CreateDirectoryIfNotExists(const QString &path);

    /**
     * Get the mime type of a file
     *
     * @param filePath The path of the file to determine
     * @return File type
     */
    static QString GetFileMimeType(const QString &filePath);
};

#endif // FILESUTILS_H
