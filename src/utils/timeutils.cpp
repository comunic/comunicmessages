#include <QDateTime>

#include "timeutils.h"

TimeUtils::TimeUtils()
{

}

QString TimeUtils::TimeDiffToString(qint64 time)
{
    qint64 diffTime = QDateTime::currentSecsSinceEpoch() - time;

    if(diffTime < 60)
        return QObject::tr("%1s ago").arg(diffTime);

    diffTime = diffTime/60;
    if(diffTime < 60)
        return QObject::tr("%1m ago").arg(diffTime);

    diffTime = diffTime/60;
    if(diffTime < 24)
        return QObject::tr("%1h ago").arg(diffTime);

    diffTime = diffTime/24;
    if(diffTime < 30){
        if(diffTime == 1)
            return QObject::tr("1 day ago");
        else
            return QObject::tr("%1 days ago").arg(diffTime);
    }

    diffTime = diffTime/30;
    if(diffTime < 12){
        if(diffTime == 1)
            return QObject::tr("1 month ago");
        else
            return QObject::tr("%1 months ago").arg(diffTime);
    }

    diffTime = diffTime/12;
    if(diffTime == 1)
        return QObject::tr("1 year ago");
    else
        return QObject::tr("%1 years ago").arg(diffTime);
}
