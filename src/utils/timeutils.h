/**
 * Time utilities
 *
 * @author Pierre HUBERT
 */

#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include <QObject>

class TimeUtils
{
public:
    TimeUtils();

    /**
     * Turn a timestamp into a diff string, in order to
     * be shown to the user
     *
     * @param time The time to convert
     * @return Generated string
     */
    static QString TimeDiffToString(qint64 time);
};

#endif // TIMEUTILS_H
