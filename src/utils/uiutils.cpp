#include <QLayout>
#include <QWidget>

#include "uiutils.h"

UiUtils::UiUtils()
{

}

void UiUtils::emptyLayout(QLayout *layout)
{
    while(layout->count() > 0){
        QLayoutItem *item = layout->itemAt(0);

        if(item->layout() != nullptr)
            emptyLayout(item->layout());

        if(item->widget() != nullptr)
            item->widget()->deleteLater();

        layout->removeItem(item);
    }
}
