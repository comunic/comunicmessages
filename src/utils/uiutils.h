/**
 * UI utilities
 *
 * @author Pierre HUBERT
 */

#ifndef UIUTILS_H
#define UIUTILS_H

class QLayout;

class UiUtils
{
public:
    UiUtils();

    /**
     * Remove all the items of a layout
     *
     * @param layout The layout to process
     */
    static void emptyLayout(QLayout *layout);
};

#endif // UIUTILS_H
