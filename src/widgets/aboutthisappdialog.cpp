#include "aboutthisappdialog.h"
#include "ui_aboutthisappdialog.h"

AboutThisAppDialog::AboutThisAppDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutThisAppDialog)
{
    ui->setupUi(this);
}

AboutThisAppDialog::~AboutThisAppDialog()
{
    delete ui;
}
