/**
 * About this app dialog
 *
 * @author Pierre HUBERT
 */

#ifndef ABOUTTHISAPPDIALOG_H
#define ABOUTTHISAPPDIALOG_H

#include <QDialog>

namespace Ui {
class AboutThisAppDialog;
}

class AboutThisAppDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutThisAppDialog(QWidget *parent = nullptr);
    ~AboutThisAppDialog();

private:
    Ui::AboutThisAppDialog *ui;
};

#endif // ABOUTTHISAPPDIALOG_H
