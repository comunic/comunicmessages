#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>

class ClickableLabel : public QLabel
{
    Q_OBJECT

public:
    ClickableLabel(QWidget *parent=nullptr);

signals:

    /**
     * This signal is emitted when the a click is made o the view
     */
    void clicked();

protected:

    void mousePressEvent(QMouseEvent*) override;

};

#endif // CLICKABLELABEL_H
