#include "conversationitemwidget.h"
#include "ui_conversationitemwidget.h"
#include "../helpers/conversationslisthelper.h"
#include "../utils/timeutils.h"

ConversationItemWidget::ConversationItemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConversationItemWidget)
{
    ui->setupUi(this);
}

ConversationItemWidget::~ConversationItemWidget()
{
    delete ui;
}

void ConversationItemWidget::setConversation(const Conversation &conv, const UsersList &list)
{
    mCurrentConversation = conv;

    ui->nameLabel->setText(ConversationsListHelper::getConversationDisplayName(conv, list));
    QFont font = ui->nameLabel->font();
    font.setBold(!conv.sawLastMessage());
    ui->nameLabel->setFont(font);

    if(conv.members().size() == 1)
        ui->numberMembersLabel->setText(tr("1 member"));
    else
        ui->numberMembersLabel->setText(tr("%1 members").arg(conv.members().size()));

    ui->lastActivityLabel->setText(TimeUtils::TimeDiffToString(conv.lastActive()));

    //Conversation not active by default
    setActive(false);
}

void ConversationItemWidget::mousePressEvent(QMouseEvent *)
{
    emit openConversation();
}

Conversation ConversationItemWidget::currentConversation() const
{
    return mCurrentConversation;
}

void ConversationItemWidget::setActive(bool active)
{
    QString styleContainer = active ? "background: black; color: white" : "";
    setStyleSheet(styleContainer);

    QString styleLabels = "padding-left: 5px;";
    ui->numberMembersLabel->setStyleSheet(styleLabels);
    ui->lastActivityLabel->setStyleSheet(styleLabels);

}
