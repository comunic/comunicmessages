/**
 * Conversation item widget
 *
 * Contains information about a single conversation
 *
 * @author Pierre HUBERT
 */
#ifndef CONVERSATIONITEMWIDGET_H
#define CONVERSATIONITEMWIDGET_H

#include <QWidget>

#include "../data/conversation.h"

namespace Ui {
class ConversationItemWidget;
}

class User;
class UsersList;

class ConversationItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConversationItemWidget(QWidget *parent = nullptr);
    ~ConversationItemWidget() override;

    /**
     * Apply a conversation to the widget
     *
     * @param conv Information about the conversation to apply
     * @param list Information about potential users of the conversation
     */
    void setConversation(const Conversation &conv, const UsersList &list);

    /**
     * Get the current conversation represented by this widget
     *
     * @return The current conversation of this widget
     */
    Conversation currentConversation() const;

    /**
     * Mark the current conversation as active or not
     *
     * @param active TRUE to mark the conversation as active /
     *      FALSE else
     */
    void setActive(bool active);

protected:

    void mousePressEvent(QMouseEvent *) override;

signals:

    /**
     * Request the conversation represented by this widget to be opened
     */
    void openConversation();

private:
    Ui::ConversationItemWidget *ui;
    Conversation mCurrentConversation;
};

#endif // CONVERSATIONITEMWIDGET_H
