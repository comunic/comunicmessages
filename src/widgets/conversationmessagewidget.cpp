#include "conversationmessagewidget.h"
#include "ui_conversationmessagewidget.h"
#include "../data/user.h"
#include "../data/conversationmessage.h"
#include "remoteimagemanager.h"
#include "clickablelabel.h"

ConversationMessageWidget::ConversationMessageWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConversationMessageWidget)
{
    ui->setupUi(this);
}

ConversationMessageWidget::~ConversationMessageWidget()
{
    delete ui;
}

void ConversationMessageWidget::setMessage(const ConversationMessage &message, const User &user)
{
    ui->nameLabel->setText(user.displayName());
    ui->messageLabel->setText(message.message());
    new RemoteImageManager(ui->accountImageLabel, user.accountImage(), this);

    //Add message image (if any)
    if(message.hasImage()){
        ClickableLabel *label = new ClickableLabel;
        label->setParent(ui->messageContentContainer);
        label->setScaledContents(true);
        label->setMaximumSize(200, 200);
        ui->messageLayout->addWidget(label);
        (new RemoteImageManager(label, message.imagePath(), this))->setEnlargeable(true);
    }
}
