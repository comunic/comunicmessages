#ifndef CONVERSATIONMESSAGEWIDGET_H
#define CONVERSATIONMESSAGEWIDGET_H

#include <QWidget>

namespace Ui {
class ConversationMessageWidget;
}

class ConversationMessage;
class User;

class ConversationMessageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConversationMessageWidget(QWidget *parent = nullptr);
    ~ConversationMessageWidget();

    /**
     * Apply a new conversation message to the widget
     *
     * @param message The message to apply to the widget
     */
    void setMessage(const ConversationMessage &message, const User &user);

private:
    Ui::ConversationMessageWidget *ui;
};

#endif // CONVERSATIONMESSAGEWIDGET_H
