#include <QMessageBox>
#include <QVBoxLayout>
#include <QTimer>

#include "conversationslistwidget.h"
#include "conversationitemwidget.h"
#include "../helpers/conversationslisthelper.h"
#include "../helpers/usershelper.h"
#include "../data/conversationslist.h"
#include "../utils/uiutils.h"
#include "../config.h"

ConversationsListWidget::ConversationsListWidget(QWidget *parent) :
    QWidget(parent)
{
    //Create conversations helper
    mConversationsList = new ConversationsListHelper(this);
    connect(mConversationsList, &ConversationsListHelper::onGotList, this, &ConversationsListWidget::onGotConversationsList);

    //Create users helper
    mUsersHelper = new UsersHelper(this);
    connect(mUsersHelper, &UsersHelper::onGotUsersInfo, this, &ConversationsListWidget::onGotUsersInfo);

    //Set conversations list layout
    new QVBoxLayout(this);

    //Automatically refresh at a regular interval the list of conversations
    mTimer = new QTimer();
    mTimer->setInterval(CONVERSATION_LIST_REFRESH_INTERVAL*1000);
    connect(mTimer, &QTimer::timeout, this, &ConversationsListWidget::refresh);
    mTimer->start();
}

ConversationsListWidget::~ConversationsListWidget()
{

}

void ConversationsListWidget::refresh()
{
    mConversationsList->getList();
}

void ConversationsListWidget::onGotConversationsList(bool success, const ConversationsList &list)
{
    qWarning("Got conversations list callback.");

    if(!success){
        QMessageBox::warning(this, tr("Error"), tr("Could not get the list of conversations!"));
        return;
    }

    //Get the list of users
    mUsersHelper->getList(list.getAllMembersId());

    //Save the list of conversations
    mCurrList = list;
}

void ConversationsListWidget::onGotUsersInfo(bool success, const UsersList &users)
{
    if(!success){
        QMessageBox::warning(this, tr("Error"), tr("Could not get information about the members of the conversations!"));
        return;
    }

    //Save members information
    mCurrList.setMembersInformation(users);

    //First, remove any present convversation
    UiUtils::emptyLayout(layout());

    //Append the list of conversations
    for(Conversation conv : mCurrList){
        ConversationItemWidget *item = new ConversationItemWidget;
        item->setConversation(conv, users);
        connect(item, &ConversationItemWidget::openConversation, this, &ConversationsListWidget::onRequestOpenConversation);
        layout()->addWidget(item);
    }

    //Make sure selected conversation is highlighted
    refreshSelectedConversation();
}

void ConversationsListWidget::onRequestOpenConversation()
{
    Conversation conversation = qobject_cast<ConversationItemWidget *>(sender())->currentConversation();

    //Notify ourselves
    setCurrentConversation(conversation);

    //Notify everybody
    emit openConversation(conversation, mCurrList.getMembersInformation());
}

void ConversationsListWidget::setCurrentConversation(const Conversation &currentConversation)
{
    mCurrentConversation = currentConversation;

    refreshSelectedConversation();
}

void ConversationsListWidget::refreshSelectedConversation()
{
    //Update UI
    for(int i = 0; i < layout()->count(); i++){
        ConversationItemWidget *widget = qobject_cast<ConversationItemWidget *>(layout()->itemAt(i)->widget());
        widget->setActive(widget->currentConversation().iD() == mCurrentConversation.iD());
    }
}
