#ifndef CONVERSATIONSLISTWIDGET_H
#define CONVERSATIONSLISTWIDGET_H

#include <QWidget>

#include "../data/conversationslist.h"
#include "../data/userslist.h"
#include "../data/user.h"

class QTimer;

class ConversationsListHelper;
class UsersHelper;

class ConversationsListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConversationsListWidget(QWidget *parent = nullptr);
    ~ConversationsListWidget();

    /**
     * This method is used to update the currently active conversation
     *
     * @param currentConversation The current conversation
     */
    void setCurrentConversation(const Conversation &currentConversation);

    /**
     * Update the UI to select the currently active conversation
     */
    void refreshSelectedConversation();

public slots:

    /**
     * Refresh the list of conversations of the user
     */
    void refresh();


signals:

    /**
     * Signal emitted when a conversation is requested to be opened
     *
     * @param conversation The conversation to open
     * @param list Information about potential required users
     */
    void openConversation(Conversation conversation, UsersList list);

private slots:

    /**
     * This slot is triggered once we have got a new list of conversations
     *
     * @param success TRUE for a success / FALSE else
     * @param list The list of conversation (empty list in case of failure)
     */
    void onGotConversationsList(bool success, const ConversationsList &list);

    /**
     * This slot is triggered once we have information about the users
     *
     * @param success TRUE in case of success / FALSE else
     * @param users The list of suers (empty list in case of failure)
     */
    void onGotUsersInfo(bool success, const UsersList &users);

    /**
     * Triggered when a conversation item request a conversation to opened
     */
    void onRequestOpenConversation();

private:
    ConversationsListHelper *mConversationsList;
    UsersHelper *mUsersHelper;

    //Current conversations list in cache
    ConversationsList mCurrList;

    //Current opened conversation
    Conversation mCurrentConversation;

    //Refresh timer
    QTimer *mTimer;
};

#endif // CONVERSATIONSLISTWIDGET_H
