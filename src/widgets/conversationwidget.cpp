#include <QMessageBox>
#include <algorithm>
#include <QTimer>
#include <QFileDialog>
#include <QScrollBar>

#include "conversationwidget.h"
#include "ui_conversationwidget.h"
#include "conversationmessagewidget.h"
#include "../helpers/conversationslisthelper.h"
#include "../helpers/conversationhelper.h"
#include "../config.h"
#include "../data/conversationmessage.h"
#include "../utils/uiutils.h"

ConversationWidget::ConversationWidget(const Conversation &conversation, const UsersList &list, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConversationWidget),
    mConversation(conversation),
    mUsersList(list)
{
    ui->setupUi(this);

    //Initialize UI
    QString convTitle = ConversationsListHelper::getConversationDisplayName(conversation, list);
    ui->convName->setText(convTitle);
    connect(ui->scrollArea->verticalScrollBar(), &QScrollBar::valueChanged, this, &ConversationWidget::messagesListScrolled);

    //Initalize helpers
    mConversationHelper = new ConversationHelper(this);
    connect(mConversationHelper, &ConversationHelper::sendMessageCallback, this, &ConversationWidget::sendMessageCallback);
    connect(mConversationHelper, &ConversationHelper::getMessagesCallback, this, &ConversationWidget::getMessagesCallback);

    //Initialize refresh timeout
    mTimer = new QTimer(this);
    connect(mTimer, &QTimer::timeout, this, &ConversationWidget::refreshTimeout);
    emit refreshTimeout();
}

ConversationWidget::~ConversationWidget()
{
    delete ui;
    delete mTimer;
}

void ConversationWidget::setMessageFormImage()
{
    //Check if an image has already been selected by the user
    if(hasUserSelectedImageToSend()){

        //Ask user confirmation
        if(QMessageBox::question(
                    this,
                    tr("Unselect image"),
                    tr("Are you sure to remove currently selected image from message ?")
            ) != QMessageBox::Yes)
            return;

        mPathToCurrentImageInForm = "";
    }

    //Pick an image
    else
        mPathToCurrentImageInForm = QFileDialog::getOpenFileName(this,
              tr("Choose image to include in the message"), "", tr("Image Files (*.png *.jpg *.jpeg, *.gif)"));

    //Check if we have an image selected
    refreshPickImageButton();
}

void ConversationWidget::sendMessage()
{
    if(isSendMessageFormLocked()){
        qInfo("sendMessage cancelled because another send message request is running...");
        return;
    }

    QString content = ui->messageContentInput->text();

    //Check message length
    if(content.length() < CONVERSATION_MESSAGE_MIN_LENGTH && !hasUserSelectedImageToSend()){
        QMessageBox::warning(this, tr("Invalid message!"), tr("Specified message is too short!"));
        return;
    }

    //Lock send form
    setSendMessageFormLocked(true);

    //Send a request to send message
    NewConversationMessage newMessage;
    newMessage.setIDConversation(mConversation.iD());
    newMessage.setMessage(content);

    //Include user image (if any)
    if(hasUserSelectedImageToSend())
        newMessage.setImagePath(mPathToCurrentImageInForm);

    //Request the message to be sent
    mConversationHelper->sendMessage(newMessage);
}

void ConversationWidget::refreshTimeout()
{
    //Check if messages are already being loaded
    if(mIsLoadingMessages)
        return;

    mIsLoadingMessages = true;
    mTimer->stop();

    //Get the latest message of the conversation
    mConversationHelper->getMessages(mConversation.iD(), mMessages.getLastMessageID());
}

void ConversationWidget::getMessagesCallback(bool success, QList<ConversationMessage> list)
{
    //Restart counter
    mTimer->start(CONVERSATION_MESSAGES_REFRESH_INTERVAL);
    mIsLoadingMessages = false;

    if(!success){
        QMessageBox::warning(this, tr("Error while getting messages list"), tr("Could not refresh messages list!"));
        return;
    }

    //Stop now if the list of messages is empty
    if(list.empty()){

        //Check if we were loading older messages
        if(mIsLoadingOlderMessages){
            mGotOldestConversationMessage = true;
            mIsLoadingOlderMessages = false;
        }

        return;
    }

    mMessages.append(list);

    std::sort(mMessages.begin(), mMessages.end());

    //Remove previous list of messages
    UiUtils::emptyLayout(ui->messagesLayout);

    //Apply the list of messages
    QList<ConversationMessage>::iterator it = mMessages.begin();
    while(it != mMessages.end()){

        ConversationMessageWidget *convMessageWidget = new ConversationMessageWidget();
        convMessageWidget->setMessage(*it, mUsersList.get(it->userID()));
        ui->messagesLayout->addWidget(convMessageWidget);

        it++;
    }

    //Scroll to the end of the widget if required
    if(list.at(list.count()-1).iD() == mMessages.getLastMessageID())
        QTimer::singleShot(1000, this, &ConversationWidget::scrollToBottom);

    //Else we can check if we reached the top of the conversation
    else if(list.count() < NUMBER_OF_OLDER_MESSAGES_TO_GET)
        mGotOldestConversationMessage = true;
}

void ConversationWidget::scrollToBottom()
{
    ui->scrollArea->verticalScrollBar()->setValue(
                ui->scrollArea->verticalScrollBar()->maximum());
}

void ConversationWidget::sendMessageCallback(bool success)
{
    setSendMessageFormLocked(false);

    if(!success){
        QMessageBox::warning(this, tr("Error"), tr("Could not send your message! Please check it and your Internet connection..."));
        return;
    }

    //Reset message form
    resetSendMessageForm();
}

void ConversationWidget::messagesListScrolled(int value)
{
    //Check if the user reached the top of the conversation
    if(value > 0)
        return; //Nothing to be done

    //Check if the conversation does not contains any message yet
    // or if we already retrieved the oldest message of the conversation
    if(mMessages.count() == 0 || mGotOldestConversationMessage)
        return;

    //Check if messsages are already being loaded
    if(mIsLoadingMessages)
        return;
    mIsLoadingMessages = true;
    mIsLoadingOlderMessages = true;

    //Get older messages
    mConversationHelper->getOlderMessages(mConversation.iD(), mMessages.getOldestMessageID());
}

void ConversationWidget::on_sendMessageButton_clicked()
{
    sendMessage();
}

void ConversationWidget::on_messageContentInput_returnPressed()
{
    sendMessage();
}

void ConversationWidget::setSendMessageFormLocked(bool lock)
{
    ui->sendMessageButton->setEnabled(!lock);
    ui->messageContentInput->setEnabled(!lock);
    ui->addImageButton->setEnabled(!lock);
}

bool ConversationWidget::isSendMessageFormLocked()
{
    return !ui->sendMessageButton->isEnabled();
}

void ConversationWidget::resetSendMessageForm()
{
    ui->messageContentInput->setText("");
    mPathToCurrentImageInForm = "";
    refreshPickImageButton();
}

void ConversationWidget::refreshPickImageButton()
{
    ui->addImageButton->setFlat(hasUserSelectedImageToSend());
}

void ConversationWidget::on_addImageButton_clicked()
{
    setMessageFormImage();
}

bool ConversationWidget::hasUserSelectedImageToSend()
{
    return !mPathToCurrentImageInForm.isEmpty();
}
