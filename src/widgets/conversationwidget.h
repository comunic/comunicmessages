/**
 * Conversation widget
 *
 * Allows to display all the components
 * of a single conversation
 *
 * @author Pierre HUBERT
 */
#ifndef CONVERSATIONWIDGET_H
#define CONVERSATIONWIDGET_H

#include <QWidget>

#include "../data/conversation.h"
#include "../data/userslist.h"
#include "../data/conversationmessage.h"
#include "../data/conversationmessageslist.h"

namespace Ui {
class ConversationWidget;
}

class ConversationHelper;

class ConversationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConversationWidget(const Conversation &conversation, const UsersList &list, QWidget *parent = nullptr);
    ~ConversationWidget();

public slots:

    /**
     * Ask the user to choose an image to send with the form
     */
    void setMessageFormImage();

    /**
     * Send the message entered by the user in the form
     */
    void sendMessage();


private slots:

    /**
     * This slot is called at regular interval in order to
     * update regulary the list of messages
     */
    void refreshTimeout();

    /**
     * Method called once we have got the list of messages
     *
     * @param success TRUE in case of success of the operation / FALSE else
     * @param list The list of downloaded messages
     */
    void getMessagesCallback(bool success, QList<ConversationMessage> list);

    /**
     * Scroll to the bottom of the conversation
     */
    void scrollToBottom();

    /**
     * Method called once the server replied to a send conversation
     * message request
     *
     * @param success TRUE in case of success / FALSE else
     */
    void sendMessageCallback(bool success);

    /**
     * Slot called when the user scroll the conversation
     *
     * @param value The new scroll value
     */
    void messagesListScrolled(int value);

    void on_sendMessageButton_clicked();

    void on_messageContentInput_returnPressed();

    void on_addImageButton_clicked();

private:

    /**
     * Check out whether the user has selected an image to include
     * to the next message he will send through the form
     *
     * @return TRUE if the user has selected an image / FALSE else
     */
    bool hasUserSelectedImageToSend();

    /**
     * Methods to get and set send message form
     * lock state
     */
    void setSendMessageFormLocked(bool lock);
    bool isSendMessageFormLocked();
    void resetSendMessageForm();
    void refreshPickImageButton();

    //Private fields
    Ui::ConversationWidget *ui;
    QString mPathToCurrentImageInForm;
    QTimer *mTimer;
    ConversationHelper *mConversationHelper;
    Conversation mConversation;
    UsersList mUsersList;
    ConversationMessagesList mMessages;
    bool mIsLoadingMessages = false;
    bool mGotOldestConversationMessage = false;
    bool mIsLoadingOlderMessages = false;
};

#endif // CONVERSATIONWIDGET_H
