#include <QMessageBox>

#include "currentuserinformationwidget.h"
#include "ui_currentuserinformationwidget.h"
#include "../helpers/accounthelper.h"
#include "../helpers/usershelper.h"
#include "../helpers/imageloadhelper.h"
#include "../data/user.h"

CurrentUserInformationWidget::CurrentUserInformationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CurrentUserInformationWidget)
{
    ui->setupUi(this);

    mUsersHelper = new UsersHelper(this);
    connect(mUsersHelper, &UsersHelper::onGotUsersInfo, this, &CurrentUserInformationWidget::onGotUsersInfo);

    //Get information about the current user
    QList<int> mUsersToGet;
    mUsersToGet << AccountHelper().getUserID();
    mUsersHelper->getList(mUsersToGet);
}

CurrentUserInformationWidget::~CurrentUserInformationWidget()
{
    delete ui;
}

void CurrentUserInformationWidget::onGotUsersInfo(bool success, const UsersList &list)
{
    if(!success || list.size() == 0){
        QMessageBox::warning(this, tr("Error"), tr("Could not get current user information!"));
        return;
    }

    User user = list.at(0);

    //Apply user information
    ImageLoadHelper::Load(ui->image_label, user.accountImage());
    ui->name_label->setText(user.displayName());
}
