/**
 * Current user information widget
 *
 * @author Pierre HUBERT
 */

#ifndef CURRENTUSERINFORMATIONWIDGET_H
#define CURRENTUSERINFORMATIONWIDGET_H

#include <QWidget>

namespace Ui {
class CurrentUserInformationWidget;
}

class UsersHelper;
class UsersList;

class CurrentUserInformationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CurrentUserInformationWidget(QWidget *parent = nullptr);
    ~CurrentUserInformationWidget();

private slots:

    /**
     * Slot called once we have got user information
     *
     * @param success Depends of the success of the operation
     * @param list
     */
    void onGotUsersInfo(bool success, const UsersList &list);

private:
    Ui::CurrentUserInformationWidget *ui;
    UsersHelper *mUsersHelper;

};

#endif // CURRENTUSERINFORMATIONWIDGET_H
