#include <QMessageBox>

#include "loginwidget.h"
#include "ui_loginwidget.h"
#include "../controllers/initcontroller.h"
#include "../utils/accountutils.h"
#include "../helpers/accounthelper.h"

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{
    ui->setupUi(this);
    mAccountHelper = new AccountHelper(this);
    connect(mAccountHelper, &AccountHelper::loginResult, this, &LoginWidget::loginResult);
}

LoginWidget::~LoginWidget()
{
    delete ui;
}

void LoginWidget::loginResult(LoginResult result)
{
    //Check if login is successfull
    if(result == LoginResult::LOGIN_SUCCESS){
        qDebug("User successfully signed in.");

        //Restart application
        (new InitController())->init();

        close();
        return;
    }

    //Release login button
    ui->loginButton->setEnabled(true);

    //Display appropriate error
    switch (result) {
        case LoginResult::INVALID_CREDENTIALS:
            showError(tr("The email and / or the password was rejected by the server!"));
            break;

        case LoginResult::NETWORK_ERROR:
            showError(tr("A network occurred. Please check your Internet connection..."));
            break;

        case LoginResult::TOO_MANY_REQUEST:
            showError(tr("Too many login attempts from your computer. Please try again later..."));
            break;

        case LoginResult::INVALID_SERVER_RESPONSE:
            showError(tr("Could not understand server response!"));
            break;

        case LoginResult::OTHER_ERROR:
        default:
            showError(tr("Login attempt did not succeed."));
            break;
    }

}

void LoginWidget::on_loginButton_clicked()
{
    submitForm();
}

void LoginWidget::on_emailEdit_returnPressed()
{
    submitForm();
}

void LoginWidget::on_passwordEdit_returnPressed()
{
    submitForm();
}


void LoginWidget::submitForm()
{
    showError("");

    QString emailAddress = ui->emailEdit->text();
    QString password = ui->passwordEdit->text();

    //Check input
    if(emailAddress.length() < 5){
       showError(tr("Please specify an email address!"));
        return;
    }

    if(password.length() < 3){
        showError(tr("Please specify a valid password!"));
        return;
    }

    if(!AccountUtils::CheckEmailAddress(emailAddress)){
        showError(("Please specify a valid email address!"));
        return;
    }

    //Block login button
    ui->loginButton->setEnabled(false);

    //Try to sign in user
    AccountLoginRequest request;
    request.setEmailAddress(emailAddress);
    request.setPassword(password);
    mAccountHelper->login(request);
}

void LoginWidget::showError(const QString &msg)
{
    ui->errorLabel->setText(msg);
}

