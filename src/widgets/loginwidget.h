#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>

#include "../data/accountloginrequest.h"

namespace Ui {
class LoginWidget;
}

class AccountHelper;


class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = nullptr);
    ~LoginWidget();

private slots:

    void loginResult(LoginResult result);

    //UI Slots
    void on_loginButton_clicked();
    void on_emailEdit_returnPressed();
    void on_passwordEdit_returnPressed();

private:

    /**
     * Submit login form
     */
    void submitForm();

    /**
     * Display an error message on the widget
     *
     * @param msg The message to show
     */
    void showError(const QString &msg);

    Ui::LoginWidget *ui;
    AccountHelper *mAccountHelper;
};

#endif // LOGINWIDGET_H
