#include <QMessageBox>
#include <QPushButton>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutthisappdialog.h"
#include "loginwidget.h"
#include "conversationslistwidget.h"
#include "conversationwidget.h"
#include "currentuserinformationwidget.h"
#include "../utils/uiutils.h"
#include "../helpers/accounthelper.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Display the list of conversations
    mConversationsListWidget = new ConversationsListWidget;
    mConversationsListWidget->refresh();
    ui->conversationsListArea->setWidget(mConversationsListWidget);
    connect(mConversationsListWidget, &ConversationsListWidget::openConversation, this, &MainWindow::openConversation);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openConversation(Conversation conversation, UsersList list)
{
    //Remove any previous conversation
    UiUtils::emptyLayout(ui->conversationsContainerLayout);
    qWarning("Open conversation");
    ConversationWidget *widget = new ConversationWidget(conversation, list);
    ui->conversationsContainerLayout->addWidget(widget);
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QApplication::aboutQt();
}

void MainWindow::on_actionAbout_this_App_triggered()
{
    AboutThisAppDialog(this).exec();
}

void MainWindow::on_actionLogout_triggered()
{
    if(QMessageBox::question(this, tr("Sign out"), tr("Do you really want to sign out from your account?")) != QMessageBox::Yes)
        return;

    AccountHelper().logout();

    //Display login widget
    (new LoginWidget())->show();

    //Close this widget
    close();
}

void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::exit(0);
}

void MainWindow::on_actionInformation_triggered()
{
    (new CurrentUserInformationWidget())->show();
}
