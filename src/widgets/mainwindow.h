/**
 * Main window of the project
 *
 * @author Pierre HUBERT
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "../data/conversation.h"
#include "../data/userslist.h"

namespace Ui {
class MainWindow;
}

class ConversationsListWidget;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    /**
     * Request a conversation to be opened
     *
     * @param conversation Information about the conversation
     * to open
     * @param list Information about the users
     */
    void openConversation(Conversation conversation, UsersList list);

    void on_actionAbout_Qt_triggered();

    void on_actionAbout_this_App_triggered();

    void on_actionLogout_triggered();

    void on_actionExit_triggered();

    void on_actionInformation_triggered();

private:
    Ui::MainWindow *ui;
    ConversationsListWidget *mConversationsListWidget;
};

#endif // MAINWINDOW_H
