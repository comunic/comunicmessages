#include <QLabel>

#include "remoteimagemanager.h"
#include "clickablelabel.h"
#include "../helpers/imageloadhelper.h"

RemoteImageManager::RemoteImageManager(QLabel *label, const QString &url, QObject *parent) : QObject(parent)
{
    //Load remote image
    ImageLoadHelper::Load(label, url);
    mUrl = url;
}

RemoteImageManager::RemoteImageManager(ClickableLabel *label, const QString &url, QObject *parent) : QObject (parent)
{
    //Load remote image
    ImageLoadHelper::Load(label, url);
    mUrl = url;

    connect(label, &ClickableLabel::clicked, this, &RemoteImageManager::enlarge);
}

bool RemoteImageManager::enlargeable() const
{
    return mEnlargeable;
}

void RemoteImageManager::setEnlargeable(bool enlargeable)
{
    mEnlargeable = enlargeable;
}

void RemoteImageManager::enlarge()
{
    if(!enlargeable())
        return;

    //Open the image in a bigger label (to improve)
    QLabel *label = new QLabel;
    ImageLoadHelper::Load(label, mUrl);
    label->show();
}
