/**
 * This object has to be used in all classes
 * in order to manage remote image management
 *
 * @author Pierre HUBERT
 */
#ifndef REMOTEIMAGEMANAGER_H
#define REMOTEIMAGEMANAGER_H

#include <QObject>

class QLabel;
class ClickableLabel;

class RemoteImageManager : public QObject
{
    Q_OBJECT
public:
    explicit RemoteImageManager(QLabel *label, const QString &url, QObject *parent); //Avoid loss of memory : force parent = nullptr);
    explicit RemoteImageManager(ClickableLabel *label, const QString &url, QObject *parent);

    /**
     * Note that the enlargable feature is currently available only with
     * the ClickableLabel widgets
     */
    bool enlargeable() const;
    void setEnlargeable(bool enlargeable);

signals:

public slots:

    /**
     * This slot is triggered when we want to enlarge the image
     */
    void enlarge();

private:

    //Private URL
    QString mUrl;
    bool mEnlargeable = false;
};

#endif // REMOTEIMAGEMANAGER_H
